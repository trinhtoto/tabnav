import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import { ifIphoneX, ifAndroid } from '../utils';
import {
  View,
  StyleSheet,
  TextInput,
  Animated,
  Text,
  TouchableOpacity,
} from 'react-native';

export default class SearchBar extends Component {

  render() {
    const { renderTabBar } = this.props;

    return (
      <View style={[styles.wrapper]}>
        <View>
          <View style={styles.searchContainer}>
            <Text style={{color:'#fff', fontSize: 20, fontWeight: 'bold', paddingHorizontal: 10}}>Viewer Page </Text>
          </View>
        </View>
        {renderTabBar()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    zIndex: 1,
    backgroundColor: '#fff',
  },
  searchContainer: {
    zIndex: 99,
    backgroundColor: '#597fab',
    width: '100%',
    overflow: 'hidden',
    paddingBottom: 10,
    ...ifIphoneX({
      paddingTop: 40
    }, {
        paddingTop: 28
      }),
  },
  arrowMinimizeContainer: {
    position: 'relative',
    top: -3,
  },
});